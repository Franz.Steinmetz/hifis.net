---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: RAFCON

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: rafcon/RAFCON_Logo.png

# One or two sentences describing the software
excerpt: RAFCON uses hierarchical state machines, featuring concurrent state execution, to represent robot programs. It ships with a graphical user interface supporting the creation of state machines and contains IDE like debugging mechanisms.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: RAFCON_statemachine.png

# Title at the top, inside the title-content-container
title: RAFCON

# Add at least one keyword
keywords:
    - visual programming
    - robotics
    - state machines
    - collaboration


# The Helmholtz research field
hgf_research_field: Aeronautics, space, and transport

# At least one responsible centre
hgf_centers:
    - German Aerospace Center (DLR)

# List of other contributing organisations (optional)
contributing_organisations:
    - Agile Robots AG

# List of scientific communities
scientific_community:
    - Robotics

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: rafcon@dlr.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://dlr-rm.github.io/RAFCON
    - type: github
      link_as: https://github.com/DLR-RM/RAFCON

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: EPL-1.0

# Is the software pricey or free? (optional)
costs: Free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Integrated development environment

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - Python

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.6561404

# Funding of the software (optional)
funding:
    name: Helmholtz, EU
    link_as: # Link (optional)
---

# RAFCON: visual state machine programming

RAFCON (RMC advanced flow control) is a software tool that allows for visual programming of hierarchical state machines, using a feature-rich graphical user interface (GUI). It is programmed in Python, which is an interpreted language that relieves one from compiling and makes it possible to alter a state machine, even while it is running. Python eases the integration of heterogeneous components of a robotic system written in different languages. The novel visualization of hierarchical state machines together with sophisticated debugging mechanisms permit fast prototyping and intuitive task development with closed loop system tests, without the need of deep programming skills. The architecture enables the collaboration in a team on a single state machine. We proved all this during the SpaceBotCamp competition.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/rafcon/RAFCON-sm-creation-preview.gif">
<span>With RAFCON, state machines can be created using an intuitive visual interface</span>
</div>

## Main features

**Universal application**
: RAFCON is written in Python, can be extended with plugins and is hard- and middleware independent.

**Visual programming**
: The sophisticated graphical editor can be used for the creation, execution and debugging of state machines.

**Collaborative working**
: Share and reuse your state machines in form of libraries, stored as JSON strings in text files.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/rafcon/complex_state_machine.png">
<span>With RAFCON, state machines can be created using an intuitive visual interface</span>
</div>

## Core

The core of RAFCON, its state machine concept, is separated from the GUI. Its API can be used for the programmatic creation of state machines. These state machines can be hierarchically organized and allow for concurrency. With libraries, state machines can be reused in other task flows. The data flow is explicitly defined using data ports and their interconnections. Also, the execution is flexible: (back-)stepping, pausing, jumping and more is possible.

## GUI

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/rafcon/GUI.png">
<span>With RAFCON, state machines can be created using an intuitive visual interface</span>
</div>

The RAFCON GUI allows to fully create and modify state machines. This is a big advantage to SMACH, for example. The modification history enables you to go back in time if errors were made. Furthermore, the user interface can be used to debug a state machine e.g. by using the execution history and the logging view. The graphical editor shows nested states smaller than their parents. Thus, unimportant details are hidden but can be accessed anytime by intuitive navigation mechanisms like zooming and panning.
