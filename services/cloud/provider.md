---
title: HIFIS for Cloud Service Providers
title_image: background-cloud.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
redirect_from:
    - hifisfor/csp
excerpt: >-
    How to hand in new service to be offered in Helmholtz Cloud.
---

<h1> Helmholtz Cloud Service Providers </h1>
<div class="image-block">
    <img
        class="help-image right"
        alt="Screenshot of Helmholtz Cloud Portal"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
        The Helmholtz Cloud is a platform of the Helmholtz Association that enables IT resources to be shared. A Helmholtz Centre can make selected IT services available so that other Hemholtz Centres can use them seamlessly and free of charge. Helmholtz-wide projects and initiatives can also participate. The services also allow to integrate external guests, so that national and international scientific collaborations are easily possible.
        </p>
    </div>
</div>

<div>
<h3> Technical and Administrative Documentation about HIFIS</h3>
    <p>
        The <a href="https://hifis.net/doc/">Documentation for HIFIS Services, Helmholtz AAI, and Helmholtz Cloud</a> summarizes all public technical and administrative documentation for the Helmholtz Federated IT Services (HIFIS) platform. It offers detailed information about Helmholtz AAI, Backbone, Cloud and Software Service, AAI Policies and Process Frameworks etc. 
    </p>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="application form in Plony"
        src="{{ site.directory.images | relative_url }}/services/application_form.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
    <h3>Becoming Service Provider</h3>
    <p>
        A service provider can hand in a service for Helmholtz Cloud at any time. The application form for potential Helmholtz Cloud Services is online available in <a href="https://plony.helmholtz-berlin.de/">Plony</a>. Detailed step-by-step instruction on the application form is available on the landing page of Plony.
        </p>
        <p>
        Helmholtz Cloud is subject to the Process Framework for the <a href="https://www.hifis.net/doc/process-framework/Chapter-Overview/">Helmholtz Cloud Service Portfolio</a> which includes the <a href="https://www.hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services/">Onboarding Process</a>. It explains how processes work, which roles are involved and how processes are interconnected. 
    </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud Service Types"
        src="{{ site.directory.images | relative_url }}services/service_types.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
    <h4>Service Types: Full Service or Pilot Service</h4>
        <p>
        A fully integrated Helmholtz service means that the service has gone through the complete onboarding process: all information is available, all interfaces are ready. But it may happen that a service is made available while this information and interfaces are still being developed, hence a pilot service. Find more information about <a href="https://www.hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.2_Service-Type-Definitions/3.2.0_Service-Type-Definitions/">fully integrated and pilot services</a>. 
        </p>       
        <h4>The Service Selection Criteria</h4>
        <p>
        In order to identify the services fitting into Helmholtz Cloud Service Portfolio, a bunch of objective <a href="https://www.hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services/#the-service-selection-criteria">service selection criteria</a> has been worked out.
        </p>
        <p>
        The criteria are divided into three types: Exclusion criteria, weighting criteria and information criteria. The application form focuses on the exclusion criteria, whereas the Service Canvas gathers the information required to evaluate the weighting and information criteria in the second step.
        </p>        
    </div>
</div>

### Helmholtz Cloud Service Portfolio
Service Portfolio Management in terms of Information Technology Infrastructure Library (ITIL) consists of three main components: 
* the Service Pipeline = [Upcoming Services](https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/)
* the Service Catalogue = currently provided Services, available on [Helmholtz Cloud Portal](https://helmholtz.cloud/services)
* the Retired Services = taken out of service

<h3>Feedback and Support</h3>
<div>
    <p>
    Your service has specific technical challenges to function as a cloud service, maybe in the fields of AAI, authorisation roles, licence limitations...? We, the HIFIS Cloud Team, are ready to help. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>


