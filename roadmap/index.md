---
title: Roadmap
title_image: default
layout: roadmap
additional_css:
  - roadmap.css
excerpt:
  "The is the roadmap of the HIFIS platform. 
   It summarises the goals for the next months and years."
---
{% comment %}
  This markdown file triggers the generation of the roadmap page.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
