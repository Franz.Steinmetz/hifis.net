---
title: SWC workshop at GFZ-Potsdam
date: 2019-12-11
authors:
  - dolling
layout: blogpost
title_image: default
excerpt_separator: <!--more-->
categories:
  - report
tags:
  - workshop
  - carpentries
additional_js:
  - mathjax.js
excerpt:
    In November the first HIFIS related Software Carpentry Workshop was held.
    <em>How hard could it be?</em>
---

# The First Workshop

### TLDR

{:.treat-as-figure}
![XKCD estimating time]({{ site.directory.images | relative_url }}posts/2019-12-11-SWC-Workshop-at-GFZ/xkcd_estimating_time.png)
Taken from [https://xkcd.com/1658/](https://xkcd.com/1658/) and licensed under
[<i class="fab fa-creative-commons fa-lg"></i>
<i class="fab fa-creative-commons-nc-eu fa-lg"></i>
<i class="fab fa-creative-commons-by fa-lg"></i>](https://creativecommons.org/licenses/by-nc/2.5/
"Creative Commons Attribution-NonCommercial 2.5 License").

In November 2019 the first HIFIS related Software Carpentry Workshop was held.
_How hard could it be?_

When I first took a look into [The Carpentries](https://carpentries.org/),
I thought: "Organizing a workshop about stuff I know - how hard can it be?"
But because the whole _The Carpentries_ idea was new to me, I wanted to
get some more information.
Luckily we had some previous workshops at the Telegrafenberg, so I
reached out to organizers and instructors of the previous ones.
In summary they told me that it takes a lot of time to organize one and prepare properly.
Okay — if you say so — I had no clue.

My colleague and I started by finding a date and a room.
Due to Christmas and the AGU conference we already realized that we had only
two months to do it. Everything seemed fine.
We met with the people that organized the workshops before to learn from their experience and get advice.
Even after that, we where pretty confident: _that's a piece of cake_.

Oh wow, were we wrong:
* The possible instructors and helpers were volunteers[^1], have commitments and thus were partially available only.
* The decision which lessons to teach depend on the availablility of instructors, so the dates, and finally available rooms.
* However, even finding a room that works best took way longer than expected, and
* The joint agreement of the lessons to teach and how to share the work load wasn't straight forward.

Finally, we started registrations around three weeks before the workshop began - which, surprisingly, has been quite enough time in advance since the workshop was overbooked within a few days.

[^1]: Thank you!

Fortunately we had about 35 registrations for 30 seats.
Lessons, staff and attendees were set a week before the workshop — _PHEW!_
We expected issues cropping up days and hours before;
They happend, but nothing to worry about.
Somehow we maneuvered ourselfs pretty well in between the pitfalls.

The only mentionable issue was, that quite a few registered people did not
participate due to different reasons: Some of them canceled a few days and hours before
because of other short-term commitments and others
were not able to submit a SINGLE slide to introduce themselves and their work
in a 60 seconds lightning talk - which has been the ticket to particpate in the workshop.
I have seen all the slides, and many of them looked like they spend less than
five minutes to quickly throw together generic pictures and five words, which
we expected and totally suited for the purpose of the slide.
However, some researchers were not able to invest these _five minutes_ for a free
_two-days_ workshop. Instead others, people from the waiting list, got the opportunity and
got the slides done on the same day they received the seat confirmation.

## On the Workshop Itself

In general, it was a great experience for all of us - the organizers, instructors,
helpers, and importantly the participants.
The lessons went well, some timing issues occured, but summarised everything was more or less
as planned.
From the feedback we got, the participants were happy and some of them even
thanked us afterwards because they learned what they needed.
One thing that we want to do better next time is staff planning.
This time, we were overstaffed with helpers. All the time we were at least two instructors,
plus two organizers and additionally three helpers, which is what we had in mind for approx. 30 participants.
Unfortunately there were 18 participants only in the end.
So somemetimes it felt like the helpers were circling around the participants in the hope of occurring problems.
First of all, the people rarely had issues, considering all the new software
and things to learn.

I am looking forward to our next workshop in April 2020!

