---
title: "Covid-19 Update: Helmholtz GPU Hackathon"
layout: event
organizers:
  - "Juckeland, Guido (HZDR)"
  - "Pleiter, Dirk (FZJ)"
title_image: group-of-people-watching-on-laptop-1595385.jpg
type:   Hackathon
start:
    date:   "2020-09-14"
    time:   "08:00"
end:
    date:   "2020-09-18"
    time:   "16:00"
location:
    campus: hida
    room:   "Seminar room"
fully_booked_out: false
registration_link: https://www.gpuhackathons.org/form/register?hackathon_which_event=194
registration_period:
    from:   "2020-04-20"
    to:     "2020-07-14"
redirect_from:
  - events/2020/01/20/helmholtz-gpu-hackathon
excerpt: 
    "GPU Hackathons are five day intensive hands-on events designed to 
    help computational scientists port their applications to GPUs using 
    libraries, OpenACC, CUDA and other tools by pairing participants with 
    dedicated mentors experienced in GPU programming and development."
---

## Description

GPU Hackathons are five day intensive hands-on events designed to help 
computational scientists port their applications to GPUs using libraries, 
OpenACC, CUDA and other tools by pairing participants with dedicated mentors 
experienced in GPU programming and development. 
Representing distinguished scholars and preeminent institutions around 
the world, these teams of mentors and attendees work together to realize 
performance gains and speedups by taking advantage of parallel programming 
on GPUs.
This event is jointly organized by Helmholtz-Zentrum Dresden-Rossendorf 
(HZDR) and Jülich Supercomputing Centre (JSC) in association with the 
Helmholtz Federated IT Services Software Cluster (HIFIS). 

### Goal
The goal of the 5-day hands-on workshop is for current or prospective user 
groups of large-scale, GPU-accelerated systems to port their applications 
to GPU accelerators or to further optimize already ported applications. 
The teams should leave with applications running on GPUs, or at least 
with a clear roadmap of how to get there. 
Access will be provided to the following GPU-accelerated supercomputers:

- The TAURUS system at TU Dresden with NVIDIA K80 GPUs and Intel Haswell 
processors as well as NVIDIA V100 GPUs and IBM POWER9 processors
- The JUWELS cluster at JSC with NVIDIA V100 GPUs and Intel Skylake processors

### Mentoring
The selected projects will receive a mentor that will prepare them and their 
application for the hackathon week and will work with them in person during 
that week. 

### Requirements
Please note that teams need to have at least three team members to ensure 
a sustainable hackathon experience for the team.
Join us for this one week of extreme immersion in GPU computing. 
Prior GPU programming knowledge is not required, but expect homework 
given before the event in this case.
If you are currently pondering if or how to move your application to GPUs, 
this event is for you and you should apply!

## Additional Information / COVID-19

The evolution of the COVID-19 pandemic and related restrictions are difficult to predict. While the hackathon is currently planned as an in-person event, we may change this to a digital event leveraging the experiences of other hackathons, which will take place in May and June 2020. At this point, we encourage you to apply even if the restrictions, which are in place today, would not allow you to attend.

If teams are participating in Berlin, space will be limited, so only six to seven teams can be accepted.
In case of over-subscription preference will be given to teams with more 
widely used, open-source applications and/or teams for which a suitable 
mentor is available.
