---
title: "TEACH Conference"
layout: event
organizers:
  - HIFIS
  - HIDA
  - HMC
  - Helmholtz Open Science Office
  - Helmholtz Talent Management
type: conference
start:
  date:   "2021-12-07"
  time:   "09:30"
end:
  date:   "2021-12-10"
  time:   "11:30"
location:
  campus: "Online"
excerpt:  "A conference on education across all communities in Helmholtz - free for all!"
registration_period:
  from:   "2021-11-30"
  to:     "2021-12-10"
registration_link:  "https://events.hifis.net/event/164/"
fully_booked_out:  "False"
---

## Let's TEACH

We invite you to join our first conference on education in Helmholtz.
A wide range of talks, discussions and workshops awaits you on a broad spectrum of education-related offers.
Check out the [full information and timetable](https://events.hifis.net/event/164/timetable/#20211207.detailed) here. 
Pick out the events that interest you and join us anytime!
