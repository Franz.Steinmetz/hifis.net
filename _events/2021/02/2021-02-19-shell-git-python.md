---
title: Software Carpentry Workshop
layout: event
organizers:
  - erxleben
lecturers:
  - lokamani
  - erxleben
type:   workshop
start:
    date:   "2021-02-19"
end:
    date:   "2021-02-22"
registration_link: https://events.hifis.net/event/44/
location:
    campus: "Online Event"
fully_booked_out: false
registration_period:
    from:   "2021-02-08"
    to:     "2021-02-14"
excerpt:
    "This basic Software Carpentry workshop will teach Shell, Git
    and Python for scientists and PhD students."
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

A [Software Carpentry workshop][swc-workshop] is conceptualized as a two-day 
event that covers the basic tools required for a research software workflow:

* The _Shell_ as a foundation for the following tools
* Employing _Git_ as version control system (VCS)
* Introduction into the _Python_ programming language

Details can be found directly at the
[workshop page][workshop-page].

## Requirements

Neither prior knowledge nor experience in those tools is needed.
Participants are asked to bring their own computer on which they can install
software.

[workshop-page]: https://events.hifis.net/event/44/
[swc-workshop]: https://software-carpentry.org/workshops/
