---
title: "Helmholtz Hacky Hour #28"
layout: event
organizers:
  - dworatzyk
type:      hacky-hour
start:
  date:   "2021-05-19"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Lost in translation</strong> Tools for rewriting code"
---
## Lost in translation
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

Rewriting code in another language or for another system can be a tedious job. We are inviting you to share your favorite tools to make this task a little bit more fun.

We are looking forward to seeing you!
