---
date: 2021-04-15
title: Tasks in April 2021
service: overall
---

## Roll out [HIFIS Survey 2021]({% link services/overall/survey.html %})
Starting data collection from Helmholtz employees about their used IT-related workflows and software tools.
The data will be analysed to further improve our services.
Results will be published later in the year.
