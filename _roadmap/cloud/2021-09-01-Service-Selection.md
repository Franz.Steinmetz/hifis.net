---
date: 2021-09-01
title: Tasks in Sept 2021
service: cloud
---

## Postponed services from initial service selection transferred into new onboarding process
Services, that were postponed during the initial service selection, as well as newly applied services were transferred into the onboarding process for new Helmholtz Cloud services. 
