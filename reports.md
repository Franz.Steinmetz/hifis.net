---
title: Reports
title_image: default
layout: default
excerpt:
    HIFIS Internal Documentation and Reports.
---
# Internal Documents
The access to the following documents (drafts) is currently restricted to HIFIS members. 

#### Technical and Administative Documentation
* [Draft of HIFIS Infrastructure Policies Draft: Sources](https://nubes.helmholtz-berlin.de/f/82025317)
* [Draft of Helmholtz Cloud Contracts](https://gitlab.hzdr.de/helmholtz-cloud-contracts/helmholtz-cloud-contract/-/tree/master)

#### Other presentations
* [10th Incubator Workshop, April 2021](https://nubes.helmholtz-berlin.de/f/254662564)
* [9th Incubator Workshop, Nov 2020](https://nubes.helmholtz-berlin.de/f/155732886)
* [HIFIS presentation at KoDa Meeting, October 2020](https://nubes.helmholtz-berlin.de/f/149595105)
* [8th Incubator Workshop, June 2020](https://nubes.helmholtz-berlin.de/f/84128581) - [PDF version](https://nubes.helmholtz-berlin.de/f/84141777)
* [Scientific Advisory Board, April 2020](https://nubes.helmholtz-berlin.de/f/79099187)

#### Reports
* [2nd Feedback Report of the Scientific Advisory Board, Aug 2021](https://nubes.helmholtz-berlin.de/f/471763329)
* [Annual report for the year 2020](https://nubes.helmholtz-berlin.de/f/174628359)
* [Annual report for the year 2019](https://nubes.helmholtz-berlin.de/f/78483551)
* [1st Feedback Report of the Scientific Advisory Board, May 2020](https://nubes.helmholtz-berlin.de/f/84005457)

#### Internal Organization
* [HIFIS Stakeholder List](https://nubes.helmholtz-berlin.de/f/41769740)
* [HIFIS Working Groups](https://gitlab.hzdr.de/hifis/communication/hifis-structure/-/blob/master/hifis_structure.md) (to be updated)
